*************
LSCSoft Conda
*************

**This repository is totally deprecated, do not attempt to use it.**

For package definitions, see https://git.ligo.org/computing/conda.git.

For documentation, see https://computing.docs.ligo.org/conda/.
